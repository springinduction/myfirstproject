package com.springboot.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootFirsAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootFirsAppApplication.class, args);
	}

}
